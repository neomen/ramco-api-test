<?php
require_once 'config.php';
require_once 'RamcoAPI.php';
$api_config = array("key" => API_KEY, "url" => API_URL, "cert" => '', "timezone_offset" => '+4 Hours');
$RamcoAPI = new RamcoAPI($api_config);

/**
 * Print result
 *
 * @param [type] $json
 * @return void
 */
function print_result($json){
  if(isset($_GET['format']) && $_GET['format']=='json'){
    print $json;
  }else{
    print "<pre>";
    print_r(json_decode($json));
    print "</pre>";
  }
}

/**
 * Clear Cache
 */
if(isset($_GET['cc'])){
  $RamcoAPI->clearVars();
  $RamcoAPI->setOperation("ClearCache");
  $json = $RamcoAPI->sendMessage();
  print_result($json);
  exit();
}

/**
 * Get All Entity Types
 */
if( !isset($_GET['type']) && !isset($_GET['fields']) ){
  $RamcoAPI->clearVars();
  $RamcoAPI->setOperation("GetEntityTypes");
  $json = $RamcoAPI->sendMessage();
  print_result($json);
}

/**
 * Get Entity fields structure
 */
if(isset($_GET['type']) && !isset($_GET['fields'])){
  $RamcoAPI->clearVars();
  $RamcoAPI->setoperation("GetEntityMetadata");
  $RamcoAPI->setEntity($_GET['type']);
  $json = $RamcoAPI->sendMessage();
  print_result($json);
}

/**
 * Get Entity and Fields
 */
if(isset($_GET['type']) && isset($_GET['fields'])){
  $RamcoAPI->clearVars();
  $RamcoAPI->setOperation("GetEntities");
  $RamcoAPI->setEntity($_GET['type']);
  $RamcoAPI->setAttributes($_GET['fields']);
  $RamcoAPI->setMaxResults("30");
  $json = $RamcoAPI->sendMessage();
  print_result($json);
  foreach (json_decode($json)->Data as $key => $value) {
    $result[$value->cobalt_eventId] = $value;
  }
  print_r($result);
}